import express from 'express'

const app = express();
const db = require('./db/data.js');
const hbs = require('hbs');

app.use(express.static(__dirname + '/public'));
app.set('views', __dirname + '/views');
app.set('view engine', 'hbs');

hbs.registerPartials(__dirname + '/views/partials');

app.get("/", (request, response) =>{
    response.render('index', {
        home: db.home[0],
        curso: db.informacion[0],
        wordCloud: db.informacion[1],
    });
})
app.get("/curso", (request, response) =>{
    response.render('curso');
})
app.get("/word_cloud", (request, response) =>{
    response.render('word_cloud');
})

const matriculas: any = [...new Set(db.integrantes.map((item: any) => item.matricula))];

app.get("/:matricula", (request, response) => {
    const matricula = request.params.matricula;
    // Se verifica si la matricula existe.
    if (matriculas.includes(matricula)) {
        const mediaFiltrada = db.media.filter((item: any) => item.matricula === matricula);
        const integrantesFiltrados = db.integrantes.filter((item: any) =>  item.matricula === matricula);
        response.render('Integrante', {
            tipoMedia: db.tipoMedia,
            integrante: integrantesFiltrados,
            media: mediaFiltrada,
        });
    }else{
        response.status(404).render('404_page');
    }
});

app.use((req, res) => {
    res.status(404).render('404_page');
});

app.listen(3000, () => {
    console.log("Server is running on port http://localhost:3000/");
})
