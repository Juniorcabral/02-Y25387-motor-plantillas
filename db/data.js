exports.integrantes = [
    { nombre: 'Junior Viviano', apellido: 'Cabral Cantero', matricula: 'Y25387', descripcion: 'Programador backend, físico y astrónomo por hobby' },
    { nombre: 'Juan José', apellido: 'Aquino Guillen', matricula: 'Y19937', descripcion: 'Estudiante de la carrera de Análisis de Sistemas Informáticos en la Universidad Católica Nuestra Señora de la Asunción sede Guarambaré' },
    { nombre: 'Sebastian', apellido: 'Pereira Duarte', matricula: 'Y25495', descripcion: 'Estudiante de la carrera de Análisis de Sistemas Informáticos en la Universidad Católica Nuestra Señora de la Asunción sede Guarambaré' },
    { nombre: 'Elvio David', apellido: 'Aguero Morel', matricula: 'Y19099', descripcion: 'Estudiante de la carrera de Análisis de Sistemas Informáticos en la Universidad Católica Nuestra Señora de la Asunción sede Guarambaré' },
    { nombre: 'Luis', apellido: 'Delgado', matricula: 'UG0085', descripcion: 'Estudiante de la carrera de Análisis de Sistemas Informáticos en la Universidad Católica Nuestra Señora de la Asunción sede Guarambaré' },
]

exports.tipoMedia = [
    { nombre: 'Imagen', titulo:'Imagen favorita', alt: 'Atardecer desde Ypané' },
    { nombre: 'Youtube' },
    { nombre: 'Dibujo', titulo:'Dibujo favorito', alt: 'Dibujo de un planeta con anillos' },
]

exports.media = [
    { src: null, url: 'https://www.youtube.com/embed/Tsnyq-3k7Bg?si=-Bzir8axv4WRU4MS&amp;controls=0' , matricula: 'Y25387', tipoMedia: 'Youtube', titulo:'Mi Video Favorito en YouTube', alt:'Video de Quantum Fracture' },
    { url: null, src: '/assets/Junior-Dibujo.png', matricula: 'Y25387', tipoMedia: 'Dibujo', titulo:'Dibujo favorito', alt: 'Dibujo de un planeta con anillos'},
    { url: null, src: '/assets/Junior-Foto.jpg', matricula: 'Y25387', tipoMedia: 'Imagen',  titulo:'Imagen favorita', alt: 'Atardecer desde Ypané'},

    {src: null, url: 'https://www.youtube.com/embed/_Yhyp-_hX2s' , matricula: 'Y19937', tipoMedia: 'Youtube', titulo:'Mi Video Favorito en YouTube', alt:'Video de Eminem'},
    {url: null, src: '/assets/Juan-Dibujo.jpg', matricula: 'Y19937', tipoMedia: 'Dibujo', titulo:'Dibujo favorito', alt: 'Dibujo representativo del basquetbol' },
    {url: null, src: '/assets/Juan-foto.jpeg', matricula: 'Y19937', tipoMedia: 'Imagen', titulo:'Imagen favorita', alt: 'Atardecer y un aro de basquet' },

    {src: null, url: 'https://www.youtube.com/embed/FRn6xXXF-7s?si=yVJP3egE0MB4siuE' , matricula: 'Y25495', tipoMedia: 'Youtube', titulo:'Mi Video Favorito en YouTube', alt:'Video del trailer de attack of titan' },
    {url: null, src: '/assets/Sebastian-Dibujo.jpg', matricula: 'Y25495', tipoMedia: 'Dibujo', titulo:'Dibujo favorito', alt: 'Dibujo de una espada en una piedra en llamas' },
    {url: null, src: '/assets/Sebastian-foto.webp', matricula: 'Y25495', tipoMedia: 'Imagen', titulo:'Imagen favorita', alt: 'Juego de Dark Soul' },

    {src: null, url: 'https://www.youtube.com/embed/NynNdkY2gx0?si=nzQjEF5y6hY6uSOp' , matricula: 'Y19099', tipoMedia: 'Youtube', titulo:'Mi Video Favorito en YouTube', alt:'Video de mejores jugadas de futbol'  },
    {url: null, src: '/assets/Elvio-Dibujo.png', matricula: 'Y19099', tipoMedia: 'Dibujo', titulo:'Dibujo favorito', alt: 'Dibujo de una cancha de futbol' },
    {url: null, src: '/assets/Elvio-Foto.jpg', matricula: 'Y19099', tipoMedia: 'Imagen', titulo:'Imagen favorita', alt: 'Cristiano Ronaldo' },

    {src: null, url: 'https://www.youtube.com/embed/Hv5ET7azgEk?si=WxryTH3oIMGTTD-Z' , matricula: 'UG0085', tipoMedia: 'Youtube', titulo:'Mi Video Favorito en YouTube', alt:'Video de la vida secreta de la mente' },
    {url: null, src: '/assets/Luis-Delgado-2.png', matricula: 'UG0085', tipoMedia: 'Dibujo', titulo:'Dibujo favorito', alt: 'Dibujo de una computadora' },
    {url: null, src: '/assets/Luis-Delgado.jpeg', matricula: 'UG0085', tipoMedia: 'Imagen', titulo:'Imagen favorita', alt: 'Foto personal estando en la cima de un cerro' },
]

exports.home = [
    { src: '/assets/logo.jpeg', alt: 'Logo del grupo', nombre: 'Devstructors', titulo: 'Inicio', descripcion: 'Integrantes: ' },
]

exports.informacion = [
    { href: '/curso', nombre: 'Información del curso' },
    { href: '/word_cloud', nombre: 'Word Cloud' },
]